var haslo="zgadnij ";
haslo=haslo.toUpperCase();

var dlugosc=haslo.length;
var iloscSkuch=0;
var haslo1="";
var yes=new Audio("muzyka/yes.mp3");
var no=new Audio("muzyka/no.mp3");
var wygrana=new Audio("muzyka/wygrana.mp3");
var przegrana=new Audio("muzyka/przegrana.mp3");

for(i=0;i<dlugosc;i++)
{
	if(haslo.charAt(i)==" ")haslo1=haslo1+" ";
	else haslo1=haslo1+"-";
}

function wypiszHaslo()
{
	document.getElementById("plansza").innerHTML=haslo1;
}

window.onload=start;

var litery= new Array(25);
litery[0]="A";
litery[1]="B";
litery[2]="C";
litery[3]="D";
litery[4]="E";
litery[5]="F";
litery[6]="G";
litery[7]="H";
litery[8]="I";
litery[9]="J";
litery[10]="K";
litery[11]="L";
litery[12]="M";
litery[13]="N";
litery[14]="O";
litery[15]="P";
litery[16]="R";
litery[17]="S";
litery[18]="T";
litery[19]="U";
litery[20]="V";
litery[21]="W";
litery[22]="X";
litery[23]="Y";
litery[24]="Z";

function start()
{
	var trescDiva="";
	for(i=0;i<=24;i++)
	{	
		var element="lit"+i;
		trescDiva=trescDiva+'<div class="litera" onclick="sprawdz('+i+')"id="'+element+'">'+litery[i]+'</div>';
		if((i+1)%6==0) trescDiva=trescDiva+'<div style="clear:both;"></div>'
	}
	
	document.getElementById("alfabet").innerHTML=trescDiva;
	wypiszHaslo();
}

String.prototype.ustawZnak=function(miejsce,znak)
{
	if(miejsce>this.length-1) return this.toString();
	else return this.substr(0,miejsce)+znak+this.substr(miejsce+1);
}

function sprawdz(nr)
{
	var trafiona=false;
	
	for(i=0;i<dlugosc;i++)
	{
		if(haslo.charAt(i)==litery[nr])
		{
			haslo1=haslo1.ustawZnak(i,litery[nr]);
			trafiona=true;
		}
	}
	
	if(trafiona==true)
	{
		yes.play();
		var element="lit"+nr;
		document.getElementById(element).style.background="#32CD32";
		document.getElementById(element).style.color="#98FB98";
		document.getElementById(element).style.border=" 3px solid #98FB98";
		document.getElementById(element).style.cursor=" default";
		wypiszHaslo();
	}
	
	else
	{
		no.play();
		var element="lit"+nr;
		document.getElementById(element).style.background="#800000";
		document.getElementById(element).style.color="#FFA07A";
		document.getElementById(element).style.border=" 3px solid #FFA07A";
		document.getElementById(element).style.cursor=" default";
		document.getElementById(element).setAttribute("onclick",";");
		
		iloscSkuch++;
		var obraz="zdjecia/s"+iloscSkuch+".jpg";
		document.getElementById("szubienica").innerHTML='<img src="'+obraz+'"alt="" />';		
	}
	
	if(haslo==haslo1)
	{
		wygrana.play();
		document.getElementById("alfabet").innerHTML = "Tak jest! Podano prawidłowe hasło: " + haslo +
		'<br/><br/><span class="reset" onclick="location.reload()">JESZCZE RAZ?</span>';		
	}

	if(iloscSkuch>=9)
	{
		przegrana.play();
		document.getElementById("alfabet").innerHTML = "Przegrana ,prawidlowe haslo: " + haslo +
		'<br/><br/><span class="reset" onclick="location.reload()">JESZCZE RAZ?</span>';	
	}	
}